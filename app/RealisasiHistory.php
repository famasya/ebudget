<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RealisasiHistory extends Model
{
    protected $table = "history";
    protected $fillable = ['month', 'year', 'value'];
}
