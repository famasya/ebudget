<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Budget;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\History;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $year = Session::get('year');
        $data = Budget::where(['year' => $year])->orderBy('id')->get();
        return view('dashboard')->with('data', $data);
    }

    public function rencana_index()
    {
        $year = Session::get('year');
        $rencana = Budget::where(['year' => $year])->orderBy('id')->get();
        $update = Budget::where(['year' => $year])->orderBy('updated_at', 'DESC')->get();
        if (count($update) != 0) {
            $last_update = $update[0]->updated_at;
        } else {
            $rencana = [];
            $m = ['januari', 'februari', 'maret', 'april', 'mei', 'juni', 'juli', 'agustus', 'september', 'oktober', 'november', 'desember'];
            for ($i = 0; $i < 12; $i++) {
                $rencana[] = (object)[
                    'month' => $m[$i],
                    'planned' => null
                ];
            }
            $last_update = '-';
        }
        return view('rencana')->with('rencana', $rencana)->with('last_update', $last_update);
    }

    public function settings_save(Request $request)
    {
        $user_id = Auth::User()->id;
        $obj_user = User::find($user_id);
        $obj_user->password = Hash::make($request->password);
        $obj_user->save();
        return redirect()->back()->with('success', 'Tersimpan');
    }

    public function realisasi_index()
    {
        $year = Session::get('year');
        $results = Budget::where(['year' => $year])->orderBy('id')->get();
        $update = Budget::where(['year' => $year])->orderBy('updated_at', 'DESC')->get();
        if (count($update) != 0) {
            $last_update = $update[0]->updated_at;
        } else {
            $last_update = '-';
        }
        return view('realisasi')->with('data', $results)->with('last_update', $last_update);
    }

    public function rencana_post(Request $request)
    {
        $year = Session::get('year');
        foreach ($request->post() as $key => $value) {
            if ($key != '_token' && $key != 'tahun') {
                $budget = Budget::where(['year' => $year, 'month' => $key])->get();
                if (count($budget) == 0) {
                    $budget = new Budget;

                    $budget->planned = preg_replace('/[^0-9]/', '', $value);
                    $budget->month = $key;
                    $budget->year = $year;
                    $budget->save();
                } else {
                    $budget = $budget[0];
                    if ($budget->planned != $value) {
                        $history = new History;
                        $history->event = 'budget_change';
                        $history->before = $budget->planned;
                        $history->after = $value;
                        $history->save();

                        $budget->planned = preg_replace('/[^0-9]/', '', $value);
                        $budget->month = $key;
                        $budget->year = $year;
                        $budget->save();
                    }
                }
            }
        }

        return redirect()->back()->with('success', 'Tersimpan');
    }

    public function laporan()
    {
        $year = Session::get('year');
        return view('laporan')->with('data', DB::table('budget')->where(['year' => $year])->orderBy('id')->get());
    }

    public function realisasi_post(Request $request)
    {
        $year = Session::get('year');
        $period = 'januari';
        foreach ($request->post() as $key => $value) {
            if ($value != null) {
                if ($key != '_token' && $key != 'tahun' && $key != 'period') {
                    $budget = Budget::find($key);

                    $realized = $budget->realized;
                    if ($realized == null) {
                        $realized = 0;
                    }

                    $history = new History;
                    $history->event = 'realized_change';
                    $history->before = $realized;
                    $history->after = $budget->realized + $value;
                    $history->month = $period;
                    $history->save();

                    $budget->realized += $value;
                    $budget->save();
                }
            }
        }

        return view('realisasi_after')->with('data', Budget::where(['year' => $year])->orderBy('id')->get())->with('data2', $request->post());
    }

}
