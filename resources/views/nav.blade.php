<!-- Navigation -->
<ul class="navbar-nav">
  <li class="nav-item ">
    <a class=" nav-link " href="/"> <i class="ni ni-tv-2 text-primary"></i> Dashboard
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="/rencana">
      <i class="ni ni-money-coins text-blue"></i> Rencana
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="/realisasi">
      <i class="ni ni-trophy text-orange"></i> Realisasi
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="/laporan">
      <i class="ni ni-bullet-list-67 text-red"></i> Laporan
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="/settings">
      <i class="ni ni-settings text-yellow"></i> Pengaturan
    </a>
  </li>
</ul>
