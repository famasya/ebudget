<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/login', function () {
  return view('login');
})->name('login');

Route::post('/login', 'LoginController@login');
Route::get('/logout', 'LoginController@logout');

Route::get('/', 'DashboardController@index');
Route::get('/settings', function () {
  return view('settings');
});
Route::get('/rencana', 'DashboardController@rencana_index');
Route::get('/laporan', 'DashboardController@laporan');
Route::get('/realisasi', 'DashboardController@realisasi_index');
Route::post('/rencana/post', 'DashboardController@rencana_post');
Route::post('/realisasi/post', 'DashboardController@realisasi_post');
Route::post('/settings/save', 'DashboardController@settings_save');
